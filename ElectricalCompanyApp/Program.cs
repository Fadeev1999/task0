﻿using System;
using ElectricalCompanyLib;
using System.Linq;

namespace ElectricalCompanyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ElectricalEnergyOrder[] orders = GetOrders();
            ElectricalEnergyCheck[] checks = new ElectricalEnergyCheck[12];
            ElectricalCompany company = new ElectricalCompany();

            for (int i = 0; i < 12; i++)
                checks[i] = company.GetCheck(orders[i]);

            SortChecksByConsumedEnergyByDescending(checks);
            SortChecksByPriceByAscending(checks);
            SortChecksByTypeOfClient(checks);

            decimal totalPayment = GetTotalPaymentForAllClients(checks);

            decimal totalBenefitForAllClients = GetTotalBenefitForAllClients(checks, company);

            Console.WriteLine($"{totalBenefitForAllClients}");
        }

        private static void SortChecksByConsumedEnergyByDescending(ElectricalEnergyCheck[] checks) =>
            checks.OrderByDescending(x => x.ConsumedEnergy);

        private static void SortChecksByPriceByAscending(ElectricalEnergyCheck[] checks) =>
            checks.OrderBy(x => x.Price);

        private static decimal GetTotalPaymentForAllClients(ElectricalEnergyCheck[] checks) =>
            checks.Sum(x => x.Price);

        private static void SortChecksByTypeOfClient(ElectricalEnergyCheck[] checks) =>
            checks.OrderBy(x => x.TypeOfClient);

        private static decimal GetTotalBenefitForAllClients(ElectricalEnergyCheck[] checks, ElectricalCompany company)
        {
            decimal totalGeneralPrice = company.GetGeneralPrice(checks.Sum(x => x.ConsumedEnergy));
            return totalGeneralPrice - GetTotalPaymentForAllClients(checks);
        }

        private static ElectricalEnergyOrder[] GetOrders() =>
             new ElectricalEnergyOrder[12]
            {
                new ElectricalEnergyOrder(TypeOfClient.General, 100),
                new ElectricalEnergyOrder(TypeOfClient.General, 170),
                new ElectricalEnergyOrder(TypeOfClient.General, 300),
                new ElectricalEnergyOrder(TypeOfClient.Limited, 100),
                new ElectricalEnergyOrder(TypeOfClient.Limited, 160),
                new ElectricalEnergyOrder(TypeOfClient.Limited, 150),
                new ElectricalEnergyOrder(TypeOfClient.Preferential1, 220),
                new ElectricalEnergyOrder(TypeOfClient.Preferential1, 30),
                new ElectricalEnergyOrder(TypeOfClient.Preferential1, 0),
                new ElectricalEnergyOrder(TypeOfClient.Preferential2, 48),
                new ElectricalEnergyOrder(TypeOfClient.Preferential2, 163),
                new ElectricalEnergyOrder(TypeOfClient.Preferential2, 272)
            };
    }
}
