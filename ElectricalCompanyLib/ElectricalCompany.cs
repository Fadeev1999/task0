using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public class ElectricalCompany
    {
        public Dictionary<TypeOfClient, IEnergyPriceAlgorithm> ClientAlgorithmMatches { get; } = new Dictionary<TypeOfClient, IEnergyPriceAlgorithm>
        {
            { TypeOfClient.General, new GeneralEnergyPriceAlgorithm() },
            { TypeOfClient.Limited, new LimitedEnergyPriceAlgorithm() },
            { TypeOfClient.Preferential1, new PreferentialEnergyPriceAlgorithm1() },
            { TypeOfClient.Preferential2, new PreferentialEnergyPriceAlgorithm2() }
        };

        public decimal GeneralUnitPrice { get; set; } = 15;

        public ElectricalEnergyCheck GetCheck(ElectricalEnergyOrder order)
        {
            decimal price = ClientAlgorithmMatches[order.TypeOfClient].GetPrice(order.ConsumedEnergy, GeneralUnitPrice);
            ElectricalEnergyCheck energyCheck = new ElectricalEnergyCheck(order.TypeOfClient, order.ConsumedEnergy, price);
            return energyCheck;
        }

        public decimal GetGeneralPrice(int consumedEnergy) =>
            ClientAlgorithmMatches[TypeOfClient.General].GetPrice(consumedEnergy, GeneralUnitPrice);
    }
}