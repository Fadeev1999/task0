using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public enum TypeOfClient
    {
        General,
        Limited,
        Preferential1,
        Preferential2
    }

    public class ElectricalEnergyCheck
    {
        public TypeOfClient TypeOfClient { get; }
        public int ConsumedEnergy { get; }
        public decimal Price { get; }
        public ElectricalEnergyCheck(TypeOfClient typeOfClient, int consumedEnergy, decimal price) =>
            (TypeOfClient, ConsumedEnergy, Price) = (typeOfClient, consumedEnergy, price);

        public override bool Equals(object obj) =>
            obj is ElectricalEnergyCheck eec && Equals(eec);

        public bool Equals(ElectricalEnergyCheck electricalEnergyCheck) =>
            TypeOfClient == electricalEnergyCheck.TypeOfClient &&
            ConsumedEnergy == electricalEnergyCheck.ConsumedEnergy &&
            Price == electricalEnergyCheck.Price;

        public override int GetHashCode() =>
            HashCode.Combine(TypeOfClient, ConsumedEnergy, Price);

        public override string ToString() =>
            $"TypeOfClient: {TypeOfClient}. ConsumedEnergy: {ConsumedEnergy}. Price: {Price}";
    }
}