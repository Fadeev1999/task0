using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public class ElectricalEnergyOrder
    {
        public TypeOfClient TypeOfClient { get; }
        public int ConsumedEnergy { get; }

        public ElectricalEnergyOrder(TypeOfClient typeOfClient, int consumedEnergy) =>
            (TypeOfClient, ConsumedEnergy) = (typeOfClient, consumedEnergy);

        public override bool Equals(object obj) =>
            obj is ElectricalEnergyOrder eeo && Equals(eeo);

        public bool Equals(ElectricalEnergyOrder electricalEnergyOrder) =>
            TypeOfClient == electricalEnergyOrder.TypeOfClient &&
            ConsumedEnergy == electricalEnergyOrder.ConsumedEnergy;

        public override int GetHashCode() =>
            HashCode.Combine(TypeOfClient, ConsumedEnergy);

        public override string ToString() =>
            $"TypeOfClient: {TypeOfClient}. ConsumedEnergy: {ConsumedEnergy}";
    }
}