using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public interface IEnergyPriceAlgorithm
    {
        decimal GetPrice(int consumedEnergy, decimal oneUnitEnergyPrice);
    }
}