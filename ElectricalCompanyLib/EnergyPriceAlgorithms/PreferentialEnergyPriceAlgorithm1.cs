using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public class PreferentialEnergyPriceAlgorithm1 : IEnergyPriceAlgorithm
    {
        private const decimal RATE = 2.0M / 3;
        public decimal GetPrice(int consumedEnergy, decimal oneUnitEnergyPrice) =>
            consumedEnergy * oneUnitEnergyPrice * RATE;
    }
}