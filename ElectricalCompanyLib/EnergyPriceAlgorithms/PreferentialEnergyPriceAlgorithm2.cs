using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public class PreferentialEnergyPriceAlgorithm2 : IEnergyPriceAlgorithm
    {
        private const int AMOUNT_OF_FREE_ENERGY = 50;

        public decimal GetPrice(int consumedEnergy, decimal oneUnitEnergyPrice) =>
            consumedEnergy <= AMOUNT_OF_FREE_ENERGY ? 0 :
                (consumedEnergy - AMOUNT_OF_FREE_ENERGY) * oneUnitEnergyPrice;   
    }
}