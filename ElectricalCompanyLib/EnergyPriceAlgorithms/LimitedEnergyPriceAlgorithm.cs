using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public class LimitedEnergyPriceAlgorithm : IEnergyPriceAlgorithm
    {
        private const decimal RATE = 4.0M / 3;
        private const int ENERGY_LIMIT = 150;
        public decimal GetPrice(int consumedEnergy, decimal oneEnergyUnitPrice)
        {
            int amountOfConsumedEnergyOverTheLimit = GetAmountOfConsumedEnergyOverTheLimit(consumedEnergy);
            int amountOfConsumedEnergyUnderTheLimit = GetAmountOfEnergyUnderTheLimit(consumedEnergy, amountOfConsumedEnergyOverTheLimit);
            return GetTotalPrice(amountOfConsumedEnergyUnderTheLimit, amountOfConsumedEnergyOverTheLimit, oneEnergyUnitPrice);
        }

        private int GetAmountOfConsumedEnergyOverTheLimit(int totalConsumedEnergy) =>
            totalConsumedEnergy <= ENERGY_LIMIT ? 0 : totalConsumedEnergy - ENERGY_LIMIT;

        private int GetAmountOfEnergyUnderTheLimit(int totalConsumedEnergy, int energyOverTheLimit) =>
            totalConsumedEnergy - energyOverTheLimit;

        private decimal GetTotalPrice(int amountOfConsumedEnergyUnderTheLimit, int amountOfConsumedEnergyOverTheLimit, decimal oneEnergyUnitPrice)
        {
            decimal price = amountOfConsumedEnergyUnderTheLimit * oneEnergyUnitPrice;

            if (amountOfConsumedEnergyOverTheLimit > 0)
                price += amountOfConsumedEnergyOverTheLimit * oneEnergyUnitPrice * RATE;

            return price;
        }
    }
}