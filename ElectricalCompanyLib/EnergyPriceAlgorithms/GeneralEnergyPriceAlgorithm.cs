using System;
using System.Collections.Generic;
using System.Linq;

namespace ElectricalCompanyLib
{
    public class GeneralEnergyPriceAlgorithm : IEnergyPriceAlgorithm
    {
        public decimal GetPrice(int consumedEnergy, decimal oneUnitEnergyPrice) =>
            consumedEnergy * oneUnitEnergyPrice;
    }
}