﻿using NuGet.Frameworks;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ElectricalCompanyLib.Tests
{
    public class GeneralEnergyPriceAlgorithmTests
    {
        [Theory]
        [InlineData(40, 15, 600)]
        [InlineData(160, 20, 3200)]
        [InlineData(130, 15, 1950)]
        public void GetPriceTest(int consumedEnergy, decimal energyUnitPrice, decimal expectedPrice)
        {
            // arrange
            GeneralEnergyPriceAlgorithm generalAlgorithm = new GeneralEnergyPriceAlgorithm();

            // act
            decimal actualPrice = generalAlgorithm.GetPrice(consumedEnergy, energyUnitPrice);

            // assert
            Assert.Equal(expectedPrice, actualPrice, 3);
        }
    }
}
