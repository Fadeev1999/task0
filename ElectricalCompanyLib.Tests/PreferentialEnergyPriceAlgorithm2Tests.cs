﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ElectricalCompanyLib.Tests
{
    public class PreferentialEnergyPriceAlgorithm2Tests
    {
        [Theory]
        [InlineData(40, 15, 0)]
        [InlineData(60, 18, 180)]
        [InlineData(130, 24, 1920)]
        public void GetPriceTest(int consumedEnergy, decimal energyUnitPrice, decimal expectedPrice)
        {
            // arrange
            PreferentialEnergyPriceAlgorithm2 generalAlgorithm = new PreferentialEnergyPriceAlgorithm2();

            // act
            decimal actualPrice = generalAlgorithm.GetPrice(consumedEnergy, energyUnitPrice);

            // assert
            Assert.Equal(expectedPrice, actualPrice, 3);
        }
    }
}
