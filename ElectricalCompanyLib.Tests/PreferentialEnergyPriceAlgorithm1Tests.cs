﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ElectricalCompanyLib.Tests
{
    public class PreferentialEnergyPriceAlgorithm1Tests
    {
        [Theory]
        [InlineData(40, 15, 400)]
        [InlineData(160, 20, 2133.3333)]
        [InlineData(130, 15, 1300)]
        public void GetPriceTest(int consumedEnergy, decimal energyUnitPrice, decimal expectedPrice)
        {
            // arrange
            PreferentialEnergyPriceAlgorithm1 generalAlgorithm = new PreferentialEnergyPriceAlgorithm1();

            // act
            decimal actualPrice = generalAlgorithm.GetPrice(consumedEnergy, energyUnitPrice);

            // assert
            Assert.Equal(expectedPrice, actualPrice, 3);
        }
    }
}
