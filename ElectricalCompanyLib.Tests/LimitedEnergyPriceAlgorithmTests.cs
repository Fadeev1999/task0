﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace ElectricalCompanyLib.Tests
{
    public class LimitedEnergyPriceAlgorithmTests
    {
        [Theory]
        [InlineData(100, 15, 1500)]
        [InlineData(170, 20, 3533.333)]
        [InlineData(150, 10, 1500)]
        public void GetPriceTest(int consumedEnergy, decimal energyUnitPrice, decimal expectedPrice)
        {
            // arrange
            LimitedEnergyPriceAlgorithm generalAlgorithm = new LimitedEnergyPriceAlgorithm();

            // act
            decimal actualPrice = generalAlgorithm.GetPrice(consumedEnergy, energyUnitPrice);

            // assert
            Assert.Equal(expectedPrice, actualPrice, 3);
        }
    }
}
